function sayHello(person: String) {
    return `hello, ${person}`;
}
let user = "Tom";
console.log(sayHello(user));

//5种基本类型
let a: number = 2;
let b: boolean = (a > 2);
let n1: number = 0b1011;//支持es6的二进制、八进制等等
let str1: string = `你好我是${a}`;
let un: undefined = undefined;
let nu: null = null;
a = undefined;//undefined、null可以赋值给任何类型，但是void不可以

function void_fun(): void { }//可以用 void 表示没有任何返回值的函数

let an1: any = "ss";
an1 = 22;//任意值的类型可以改变，类似于js
let an2 = "ddd";//变量如果在声明的时候，未指定其类型，那么它会被识别为任意值类型：

let an3;//an3为any类型
let an4 = 1;//an4后续只能为number类型
an3 = "dsd";
an3 = 333;
console.log(an3);
an4 = 0;

let union: string | number | boolean = 23;
function set_union(a: number | string): number {
    return a.toString().length;
}

interface my_interface {
    readonly name: string;
    age: number;
    is_graduated: boolean | number;
    has_boyfriend?: boolean;
    [propName: string]: number | boolean | string;
}
let tom: my_interface = {
    name: "tom",
    age: 24,
    is_graduated: 1
}

//数组的4种表示方法
let arr1: number[] = [1, 3, 4];//类型+方括号[]
let arr2: Array<number> = [1, 1, 3, 4, 7];//泛型

interface like_arr {//接口表示数组（不常用，但是常用来表示类数组）
    [index: number]: any;
    length?: number;//可选属性，可以表示数组，又可以表示类数组
}
let arr4: like_arr = [1, 3, 4, 5];
console.log(arr4);
function arr_like(arg1: string | number, arg2: number): void {
    let args: like_arr = arguments;//接口表示类数组
    console.log(args);
}
arr_like(1, 3);

let fun1 = function (arg1: string, arg2: string | number): number {
    return parseInt(arg1) + parseInt(arg2.toString());
}
let fun2: (arg1: string, arg2: string | number) => number = function (arg1: string, arg2: string | number): number {
    return parseInt(arg1) + parseInt(arg2.toString());
}

interface fun_inter {
    (arg1: number, arg2: number): number;
}
let fun3: fun_inter;//接口定义函数形状
fun3 = function (arg1: number, arg2: number, arg3?: number, arg4: number = 1): number {
    return arg1 + arg2;
}
console.log(fun3(1, 3));

let push = function (arr: any[], ...rest: any[]) {
    rest.forEach(function (item: any): void {
        arr.push(item);
    })
}
let arr5: any[] = [];
push(arr5, 1, 3, 4, 5, '3');
console.log(arr5);

function assertion1(arg1: number[] | number): string {
    return (arg1 as number[]).join(','); //传入非number[]类型的参数会报错
}
console.log(assertion1([22, 33]));

// window.age = 222; //报错
//正确写法
// (window as any).age = 222;
// console.log((window as any).age);


interface cat {
    name: string,
    run: string
}
interface fish {
    name: string,
    swim: string
}
function compare(obj: cat | fish): string {
    if ((obj as cat).run) {
        return (obj as cat).run;
    }
    else return (obj as fish).swim;
}
const my_cat1: cat = {
    name: "Tom",
    run: "run!"
}
console.log(compare(my_cat1));
const test = my_cat1 as any as fish;
// console.log(test.swim);//undefined

interface animals {
    name: string
}
const some_animal: animals = {
    name: 'dragon'
}

let v_cat1: cat = some_animal as cat;//animals兼容cat，可以互相断言
let v_animal1: animals = my_cat1 as animals;

// let v_cat2: cat = some_animal;//报错，类型转换cat不兼容animals
let v_animal2: animals = my_cat1;//正确，animals兼容cat


//泛型
function create_array<T, T1, T2>(length: number, value: T): Array<T> {
    let result = [];
    for (let i = 0; i < length; i++) {
        result[i] = value;
    }
    return result;
}
console.log(create_array(5, 44));

interface length_arg {
    length: number;
}
function test_generics2<T extends length_arg>(arg: T): void { }
// test_generics2(0);//报错，0不包含length属性
test_generics2("");

function test_generics3<T extends T1, T1>(child: T, parent: T1): void { }
test_generics3({ name: 'zouzhu', age: 24 }, { name: 'zouzhu' })// 第一个参数是第二个参数的子类

//使用泛型定义一个函数的形状
interface my_array<T> {
    (length: number, value: T, ext?: any): Array<T>;
}
let create_my_array: my_array<any>;//泛型接口
create_my_array = function <T>(length: number, value: T): Array<T> {
    let result = [];
    for (let i = 0; i < length; i++) {
        result.push(value);
    }
    return result;
}
console.log(create_my_array(4, '你好'));

class my_class<T = number>{//默认的类型为number
    public inner_array: T;
    print() {
        console.log(typeof this.inner_array);
    }
    constructor(arg: T) {
        this.inner_array = arg;
    }
}
let ob1 = new my_class<number>(32);//由构造函数可知这里的参数类型必须和泛型类型一致
ob1.print();

//声明文件
declare let jQuery: (selector: string) => any;
// console.log(jQuery);//如果没有declare语句会报错

declare class declare_class {
    name: string;
    say(): void;
}
declare namespace my_namespace {
    const version: string;
    class space_class {
        name: string;
        say(): void;
    }
    interface inner_inter {
        method?: 'GET' | 'POST';
        data?: any;
    }
    function my_fun(): void;
}


interface my_interface2 {
    name: number;
}
//类型别名
type my_string = string;
type my_inter = my_interface | my_interface2;
type str_list = 'str1' | 'str2' | 'zouzhu';
function test_str(arg: str_list): void { }
// test_str('zou');//报错，必须是str_list字符串字面量类型中的一个
test_str('str1');//正确


//元组
// let tom1: [string, number, boolean] = ["", 22, false, 44];//报错，初始化赋值的时候，元素个数不能越界
let tom1: [string, number] = ["", 22];
tom1.push(233);//虽然越界了，但是不会报错，
// tom1.push(true);//报错，添加的越界元素必须是元组种每个类型的联合类型
// console.log(tom1);
tom1[0] = 'Tom';
tom1[1] = 25;

tom1[0].slice(1);
tom1[1].toFixed(2);
// console.log(tom1);

//枚举
enum my_enum1 { one = 999, two, three, four, five = <any>'邹竹', six = <any>'me'.split('') };
console.log(`one:${my_enum1['one']}  two:${my_enum1['two']}  three:${my_enum1['three']}   six:${JSON.stringify(my_enum1['six'])}`);// 999 1000 1001 ["m","e"]
const enum my_const_enum1 { up = 1, down, left, right = "d" };


//类
class People {
    public readonly name: string = 'zouzhu';
    private has_boyfriend: boolean;
    protected is_rich: boolean;
    constructor(name?: string) {
        name ? this.name = name : '';
        this.name = 'zack';//构造函数中可以修改readonly值
    }
    change_name(new_name: string): void {
        // this.name = new_name;//报错，readonly属性只能在构造函数中修改，其他方法/实例.属性的方式都不能修改readonly属性值
    }
}
let x = new People();
console.log(x);//{name: "zack"}

abstract class creature {
    public readonly name: string;
    protected age: number = 24;
    constructor(name: string) {
        this.name = name;
    }
    public abstract hello(): void;
}
class human extends creature {
    constructor(name: string) {
        super(name);
    }
    public hello(): void {
        console.log(`Hi, I am ${this.name}, age ${this.age}`);
    }
}
// let me: creature = new human('zouzhu');//正确，因为子类型实例可以赋值给父类型变量
let me: human = new human('zouzhu');
me.hello();

//类与接口
interface water {
    water_prop: string;
    swim(): void;
}
interface land_father {
    land_prop: string;
}
interface land extends land_father {//接口可以继承接口
    run(): void;
}
class frog implements water, land {
    land_prop: string = "跑步";
    water_prop: string = "游泳";
    constructor(other_prop?: string) {//构造函数不会被接口继承，同理静态属性、静态方法也不会被接口继承
        other_prop ? this.other_props = other_prop : "";
    }
    swim(): void {
        console.log("I'm swiming!");
    }
    run(): void {
        console.log("I'm running!");
    }
    other_reaction?(): void {
        console.log("Other reaction!");
    }
    other_props?: string;
}
let f1 = new frog();
f1.swim();
f1.run();
f1.other_reaction();
console.log(f1.water_prop, f1.land_prop);
//接口可以继承类
interface child_inter extends frog { }
let child_obj: child_inter = {//接口可以继承类
    land_prop: "",
    water_prop: "",
    swim() { },
    run() { },
    other_props: "额外的属性"
}
