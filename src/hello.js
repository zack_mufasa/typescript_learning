function sayHello(person) {
    return `hello, ${person}`;
}
let user = "Tom";
console.log(sayHello(user));
let a = 2;
let b = (a > 2);
let n1 = 0b1011;
let str1 = `你好我是${a}`;
let un = undefined;
let nu = null;
a = undefined;
function void_fun() { }
let an1 = "ss";
an1 = 22;
let an2 = "ddd";
let an3;
let an4 = 1;
an3 = "dsd";
an3 = 333;
console.log(an3);
an4 = 0;
let union = 23;
function set_union(a) {
    return a.toString().length;
}
let tom = {
    name: "tom",
    age: 24,
    is_graduated: 1
};
let arr1 = [1, 3, 4];
let arr2 = [1, 1, 3, 4, 7];
let arr4 = [1, 3, 4, 5];
console.log(arr4);
function arr_like(arg1, arg2) {
    let args = arguments;
    console.log(args);
}
arr_like(1, 3);
let fun1 = function (arg1, arg2) {
    return parseInt(arg1) + parseInt(arg2.toString());
};
let fun2 = function (arg1, arg2) {
    return parseInt(arg1) + parseInt(arg2.toString());
};
let fun3;
fun3 = function (arg1, arg2, arg3, arg4 = 1) {
    return arg1 + arg2;
};
console.log(fun3(1, 3));
let push = function (arr, ...rest) {
    rest.forEach(function (item) {
        arr.push(item);
    });
};
let arr5 = [];
push(arr5, 1, 3, 4, 5, '3');
console.log(arr5);
function assertion1(arg1) {
    return arg1.join(',');
}
console.log(assertion1([22, 33]));
function compare(obj) {
    if (obj.run) {
        return obj.run;
    }
    else
        return obj.swim;
}
const my_cat1 = {
    name: "Tom",
    run: "run!"
};
console.log(compare(my_cat1));
const test = my_cat1;
const some_animal = {
    name: 'dragon'
};
let v_cat1 = some_animal;
let v_animal1 = my_cat1;
let v_animal2 = my_cat1;
function create_array(length, value) {
    let result = [];
    for (let i = 0; i < length; i++) {
        result[i] = value;
    }
    return result;
}
console.log(create_array(5, 44));
function test_generics2(arg) { }
test_generics2("");
function test_generics3(child, parent) { }
test_generics3({ name: 'zouzhu', age: 24 }, { name: 'zouzhu' });
let create_my_array;
create_my_array = function (length, value) {
    let result = [];
    for (let i = 0; i < length; i++) {
        result.push(value);
    }
    return result;
};
console.log(create_my_array(4, '你好'));
class my_class {
    constructor(arg) {
        this.inner_array = arg;
    }
    print() {
        console.log(typeof this.inner_array);
    }
}
let ob1 = new my_class(32);
ob1.print();
console.log(jQuery);
let tom1 = ["", 22];
tom1.push(233);
tom1[0] = 'Tom';
tom1[1] = 25;
tom1[0].slice(1);
tom1[1].toFixed(2);
