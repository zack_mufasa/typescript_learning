let n: number = null;
n = 23;
let s: string = "";
s = undefined;
let b1: boolean = true;
// b1=s as any as void;//双重断言，报错，不能将void赋值给其他类型
let fn: (arg1: string, arg2: number) => void = function (arg1: string, arg2: number): void {
    console.log(arg1 + arg2);
}
let fn2: (arg1: number | string) => string = function (arg1: number): string {
    return arg1.toString();
}
let fn_generics: <T>(arg1: T, arg2: string) => T = function <T>(arg1: T, arg2: string) {
    return 1 as any as T;
}
function fn3(arg1: string): string {
    return arg1;
}
function fn4<T = string>(arg1: T, arg2: number): T {
    return arg1;
}
let x2 = 12;
// x2="";//报错,类型推论已经把x2推论为number类型
x2 = 22;

interface my_interface3 {
    p1: string;
    p2: number | string;
    p_f(arg1: string): void;//注意和line 8函数的定义语法相区分
}
interface single_fun {
    (arg1: string): number;
}
let ob2: my_interface3 = {
    p1: "a",
    p2: 1,
    p_f(arg1: string) { }
}
let fn5: single_fun = function (arg1: string): number { return 2; }


let arr6: string[] = [];
let arr7: Array<string> = ["", "1"];

//元组
let un1: [string, number] = ["", 33];
let un2: [boolean, string, number] = [true, undefined, 89];

let my_any;
my_any = 1;
my_any = "123";
console.log(my_any);