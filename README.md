[TOC]
## 简介
### 对比JS
1. 添加了类型系统的 JavaScript
2. TypeScript是**静态类型**，编译阶段就报错；JavaScript是动态类型，没有编译阶段，执行时才会类型报错
3. TypeScript 可以编译为 JavaScript
4. TypeScript 非常适用于大型项目——类型系统让大型项目更易维护、更少bug
5. TypeScript 拥有很多编译选项，类型检查的严格程度由你决定 
6. TS支持ES6
7. TypeScript和JavaScript都是弱类型：「是否允许隐式类型转换」来分类，可以分为弱类型（允许）和强类型（不允许）

### 安装和编译
1. 安装、编译
```javascript
npm install -g typescript //安装
tsc --init //生成一个tsconfig.json配置文件
tsc //编译，默认会编译项目下所有的ts文件，可以通过配置tsconfig.json中的exclude或者include字段来控制编译文件
tsc hello.ts //tsc编译hello.ts为hello.js
```
2. 编译时进行类型检查，即使编译出错，也会生成结果文件

## 基础
### 5种基本数据类型+any任意值：**any类型上访问任何属性/任何方法都是可以的**
1. **undefined、null可以赋值给任何类型，但是void不可以**
    ```typescript
    //5种基本类型
    let a: number = 2;
    let b: boolean = (a > 2);
    let n1: number = 0b1011;//支持es6的二进制、八进制等等
    let str1: string = `你好我是${a}`;
    let un: undefined = undefined;
    let nu: null = null;
    a = undefined;//undefined、null可以赋值给任何类型，但是void不可以
    function void_fun(): void { }//可以用 void 表示没有任何返回值的函数

    let an1: any = "ss";
    an1 = 22;//任意值的类型可以改变，类似于js
    let an2 = "ddd";//变量如果在声明的时候，未指定其类型，那么它会被识别为任意值类型：
    ```
### 类型推论
1.   对于明确指定类型的，如果声明的时候：
        *  声明同时赋值：被推测为赋值的类型
        *  只声明不赋值：被推测为any，不进行类型检查
        ```typescript
        let an3;//an3为any类型
        let an4 = 1;//an4后续只能为number类型
        an3 = "dsd";
        an3 = 333;
        ```
### 联合类型
1. 变量可以指定多种类型，使用 | 分隔每个类型：`let union : string | number | boolean = 23;`
2. **不确定联合类型到底是何种类型的时候**，调用类型的属性或者方法必须各类型**共有**的，否则会报错
    ```typescript
    function set_union(a: number | string): number {
        return a.toString().length;//.toString()是number和string类型共有的方法
    }
    ```
3. 联合类型赋值后也会进行类型推论，被推测为赋值的类型
### **对象的类型：接口**
1. 接口对**类**的形状（属性）和行为（所以也可以包含方法）进行抽象，类的形状必须和接口**完全一致**,属性比接口形状多/少都会报错
2. 可以使用**可选属性**`?:`来定义某些**不确定是否**存在的属性
```typescript
interface my_interface {
    readonly name: string;
    age: number;
    is_graduated: boolean | number;
    has_boyfriend?: boolean;
    [propName: string]: number | boolean | string;
}
let tom: my_interface = {
    name: "tom",
    age: 24,
    is_graduated: 1
}
interface my_interface2 {
    fun(arg: string): void;
}
```
3. 任意属性：使用` [propName: string]: number `定义任意属性取 number 类型的值，其他属性必须是任意属性类型限制类型的子集
4. 只读属性：在接口的属性前加` readonly `修饰符，**第一次给对象赋值的时候就必须给只读属性赋值！** 并且后续不可以改动
### 数组类型：4种数组表示方法
```typescript
//数组的4种表示方法
let arr1:number[]=[1,3,4];//类型+方括号[]
let arr2:Array<number>=[1,1,3,4,7];// **泛型表示数组

interface like_arr {//接口表示数组（不常用，但是常用来表示类数组）
    [index: number]: any;
    length?: number;//可选属性，可以表示数组，又可以表示类数组
}
let arr4: like_arr = [1, 3, 4, 5];
console.log(arr4);
function arr_like(arg1: string | number, arg2: number): void {
    let args: like_arr = arguments;//接口表示类数组
    console.log(args);
}
arr_like(1, 3);
```
### 函数类型
1. 函数表达式，完整的函数表达式如下
```typescript
let fun2: (arg1: string, arg2: string) => number = function (arg1: string, arg2: string): number {
    return parseInt(arg1) + parseInt(arg2.toString());
}
```
2. 在 TypeScript 的类型定义中，=> 用来表示函数的定义，左边是输入类型，需要用括号括起来，右边是返回值类型。与ES6的箭头函数不是一个东西。
```typescript
(输入的参数以及类型) => 返回类型
```
3. 接口定义函数的形状+可选参数+参数默认值：可选参数必须放在所有必选参数后面，否则报错
```typescript
interface fun_inter {
    (arg1: number, arg2: number): number;
}
let fun3: fun_inter;//接口定义函数形状
fun3 = function (arg1: number, arg2: number, arg3?: number, arg4: number = 1): number {
    return arg1 + arg2;
}
console.log(fun3(1, 3));

let push = function (arr: any[], ...rest: any[]) {
    rest.forEach(function (item: any): void {
        arr.push(item);
    })
}
let arr5: any[] = [];
push(arr5, 1, 3, 4, 5, '3');
console.log(arr5);
```
4. 重载：使用重载定义多个 reverse 的函数类型，精确的表达，输入为数字的时候，输出也应该为数字，输入为字符串的时候，输出也应该为字符串。
```typescript
function reverse(x: number): number;
function reverse(x: string): string;
function reverse(x: number | string): number | string | void {
    if (typeof x === 'number') {
        return Number(x.toString().split('').reverse().join(''));
    } else if (typeof x === 'string') {
        return x.split('').reverse().join('');
    }
}
```
### 类型断言（手动指定一个类型）
兼容：存在两个接口A、B，其中A包含的属性/方法在B内都能找到，此外B可能还会有自己额外的属性/方法，则A兼容B，类似于父类兼容子类
**子类实例可以赋值给父类变量，父类实例不能赋值给子类变量**
1. 语法
```typescript
值 as 类型 //推荐
//or
<类型>值   //不推荐，可能和泛型混淆，jsx语法也会歧义
```
2. 用途
    1. 将一个联合类型（class1 | class2）**断言**成其中的一个类型，比如class1。使用断言后避免调用方法或者使用深层次属性
    ```typescript
    function assertion1(arg1: number[] | number): string {
        return (arg1 as number[]).join(','); //传入非number[]类型的参数会报错
    }
    console.log(assertion1([22, 33]));
    ```
    2. 将一个父类**断言**成一个更具体的子类
    ```typescript
    interface ApiError extends Error {
        code: number;
    }
    interface HttpError extends Error {
        statusCode: number;
    }

    function isApiError(error: Error) : boolean {
        if (typeof (error as ApiError).code === 'number') {
            return true;
        }
        return false;
    }
    ```
    3. 将任何类型断言成`any`：将一个变量断言为 any 可以说是解决 TypeScript 中类型问题的最后一个手段。它极有可能掩盖了真正的类型错误，所以如果不是非常确定，就不要使用`as any`。不能滥用`as any`，另一方面也不要完全否定它的作用
    ```typescript
    // window.age = 222; //报错
    (window as any).age = 222;
    console.log((window as any).age);
    ```
    4. 将any断言成一个具体类型，比如函数fun返回值为any，调用该函数时使用`const result = fun() as Cat`，其中，Cat是一个接口；提高代码的可维护性
3. 阶段总结
    1. 联合类型可以被断言为其中一个类型
    2. 父类可以被断言为子类 / 子类可以被断言成父类
    3. 任何类型都可以被断言为 any
    4. any 可以被断言为任何类型
    5. 要使得 A 能够被断言为 B，只需要 A 兼容 B 或 B 兼容 A 即可
4. 类型断言的限制
    1. 不是所有类型都可以互相断言的，需要二者**兼容**，比如：Cat包含Animals的所有属性/方法，此外还有自己的独特的方法/属性，则称Animals兼容Cat，彼此可以互相断言
5. 双重断言
    1. `my_cat1 as any as fish` 将任何一个类型通过`as any as`作为中转转换成另外一个类型
6. 类型断言vs类型声明（类型声明比类型断言更加严格）
    1. 对于animals、cat来说，类型断言只需要其中一者兼容另外一者即可，但是类型转换需要目标类型兼容被转换变量的类型
    ```typescript
    interface cat {
        name: string,
        run: string
    }
    interface animals {
        name: string
    }
    //some_animal和my_cat1赋值操作省略
    let v_cat1: cat = some_animal as cat;//正确，animals兼容cat，可以互相断言
    let v_animal1: animals = my_cat1 as animals;

    // let v_cat2: cat = some_animal;//报错，类型转换cat不兼容animals
    let v_animal2: animals = my_cat1;//正确，animals兼容cat
    ```
    2. 为了增加代码的质量，我们最好优先使用类型声明，这也比类型断言的 as 语法更加优雅
7. 类型断言vs泛型：最优解决方案→泛型
```typescript
//优化前：类型断言
function getCacheData(key: string): any {
    return (window as any).cache[key];
}
interface Cat {
    name: string;
    run(): void;
}
const tom = getCacheData('tom') as Cat;
tom.run();

//优化后：泛型
function getCacheData<T>(key: string): T {
    return (window as any).cache[key];
}
const tom = getCacheData<Cat>('tom');
tom.run();
```
### 声明文件
1. 对于我们引入的第三方库比如jQuery，要直接在TS中使用必须要用declare声明，否则TS无法使识别：declare仅用于编译时的检查，编译结果中会被删除
2. 使用declare来声明一个变量/函数/类/接口/枚举类型/命名空间等
```typescript
declare let jQuery: (selector: string) => any;
//function/class/interface/enum/namespace/type...
declare class declare_class {
    name: string;
    say(): void;
}
declare namespace my_namespace {
    const version: string;
    class space_class {
        name: string;
        say(): void;
    }
    function my_fun(): void;
}
```
3. 通常将所有的声明语句放到一个`.d.ts`（必须是这个后缀）后缀的文件中
4. 由于声明文件只是为了编译检查的时候使用，因此里面**只包含各种类型/形状**，不会有其具体实现，如果写了具体实现会报错，其具体实现应该是在引进的第三方库中实现的
//后续待补充...


### 内置对象
1. ECMAScript标准的内置类型：Boolean、Error、Date、RegExp等，可以将变量设置为这些类型
2. DOM 和 BOM 提供的内置对象有：Document、HTMLElement、Event、NodeList 等。


## 进阶
### 类型别名：type + 字符串字面量类型
```typescript
//为string类型和（my_interface|my_interface2）联合类型取别名
type my_string = string;
type my_inter = my_interface | my_interface2;

type str_list = 'str1' | 'str2' | 'zouzhu';
function test_str(arg: str_list): void { }
// test_str('zou');//报错，必须是str_list字符串字面量类型中的一个
test_str('str1');//正确
```
### 枚举：`enum`
1. 枚举内部成员的值默认为数字0开始递增，并且可以反向映射
2. 未手动赋值的枚举项会**接着**上一个枚举项递增
3. 成员赋值重复不会被ts察觉，所以不要有覆盖的情况
4. 枚举项有两种类型：常数项（只能是一个直接的数值/字符串等）和计算所得项（可以有计算），使用`const enum`声明一个只有常数项的枚举
```typescript
enum my_enum1 { one = 999, two, three, four, five = <any>'邹竹', six = <any>'me'.split('') };
console.log(`one:${my_enum1['one']}  two:${my_enum1['two']}  three:${my_enum1['three']}   six:${JSON.stringify(my_enum1['six'])}`);// 999 1000 1001 ["m","e"]
const enum my_const_enum1 { up = 1, down, left, right = "d" };//const enum成员不可以包含如.length、.split之类的计算
```

### 元组：[string, number, boolean]
1. 数组合并了相同类型的对象，而元组（Tuple）合并了不同类型的对象。
```typescript
// let tom1: [string, number, boolean] = ["", 22, false, 44];//报错，初始化赋值的时候，元素个数不能越界
let tom1: [string, number] = ["", 22];
tom1.push(233);//虽然越界了，但是不会报错，
// tom1.push(true);//报错，添加的越界元素必须是元组种每个类型的联合类型
console.log(tom1);
tom1[0] = 'Tom';
tom1[1] = 25;

tom1[0].slice(1);
tom1[1].toFixed(2);
console.log(tom1);
```

### 类
1. 三大特性
    1. 封装：隐藏细节，只暴露对外接口
    2. 继承：子类继承父类
    3. 多态：对父类的同一个方法不同子类有不同的实现
2. 三种访问修饰符：public、protected、private
    1. public是公开的，外部可访问
    2. protected是受保护的，只有**类内部和子类内部**可以访问
    3. private私有的，只有本类内部可以访问
3. readonly修饰符：写在其它修饰符最后，并且实例不可以修改该属性值
4. abstract修饰符：抽象类、抽象方法
    1. 抽象类是不允许被实例化的
    2. **抽象方法在子类中必须被实现**，抽象方法不能定义在非抽象类上
5. 继承：与ES6一样使用extends
```typescript
abstract class creature {
    public readonly name: string;
    protected age: number = 24;
    constructor(name: string) {
        this.name = name;
    }
    public abstract hello(): void;
}
class human extends creature {
    constructor(name: string) {
        super(name);
    }
    public hello(): void {//不实现hello方法会报错
        console.log(`Hi, I am ${this.name}, age ${this.age}`);
    }
}
// let me: creature = new human('zouzhu');//正确，因为子类型实例可以赋值给父类型变量
let me: human = new human('zouzhu');
me.hello();
```
### 类与接口
1. 将多个类的共同行为/属性提取成接口，类定义时通过`implements`实现接口
2. 接口可以继承接口
3. **接口也可以继承类**：接口其实继承的是这个类的**类型（类中除了构造函数、静态方法/属性以外的部分）**，也就是其实例属性和实例方法
```typescript
interface water {
    water_prop: string;
    swim(): void;
}
interface land_father {
    land_prop: string;
}
interface land extends land_father {//接口可以继承接口
    run(): void;
}
class frog implements water, land {
    land_prop: string = "跑步";
    water_prop: string = "游泳";
    constructor(other_prop?: string) {//构造函数不会被接口继承，同理静态属性、静态方法也不会被接口继承
        other_prop ? this.other_props = other_prop : "";
    }
    swim(): void {
        console.log("I'm swiming!");
    }
    run(): void {
        console.log("I'm running!");
    }
    other_reaction?(): void {
        console.log("Other reaction!");
    }
    other_props?: string;
}
let f1 = new frog();
f1.swim();
f1.run();
f1.other_reaction();
console.log(f1.water_prop, f1.land_prop);
//接口可以继承类
interface child_inter extends frog { }
let child_obj: child_inter = {//接口可以继承类
    land_prop: "",
    water_prop: "",
    swim() { },
    run() { },
    other_props: "额外的属性"
}
```


### 泛型 
1. 在函数后添加`<T>`，T用于表示任意的输入类型，可以定义返回值的类型为：`Array<T>`来限制返回值的每一项都为同一个类型（T类型），而避免使用any允许每一项都为any类型
2. 使用`<T, T1, T2>`形式来定义多个类型的参数
```typescript
function create_array<T, T1, T2>(length: number, value: T): Array<T> {
    let result = [];
    for (let i = 0; i < length; i++) {
        result[i] = value;
    }
    return result;
}
console.log(create_array(5, 44));
```
3. 泛型约束，限制泛型的类型/形状，使用extends+interface
    1. 直接约束
    2. 各个参数之间也可以互相约束
    ```typescript
    interface length_arg {
        length: number;
    }
    function test_generics2<T extends length_arg>(arg: T): void { }
    // test_generics2(0);//报错，0不包含length属性
    test_generics2("");

    function test_generics3<T extends T1, T1>(child: T, parent: T1): void { }
    test_generics3({ name: 'zouzhu', age: 24 }, { name: 'zouzhu' })// 第一个参数是第二个参数的子类
    ```
4. 泛型接口
```typescript
//使用泛型定义一个函数的形状
interface my_array<T> {
    (length: number, value: T, ext?: any): Array<T>;
}
let create_my_array: my_array<any>;//泛型接口
create_my_array = function <T>(length: number, value: T): Array<T> {
    let result = [];
    for (let i = 0; i < length; i++) {
        result.push(value);
    }
    return result;
}
console.log(create_my_array(4, '你好'));
```
5. 泛型类 + 泛型参数默认类型
```typescript
class my_class<T = number>{//默认的类型为number
    public inner_array: T;
    print() {
        console.log(typeof this.inner_array);
    }
    constructor(arg: T) {
        this.inner_array = arg;
    }
}
let ob1 = new my_class<number>(32);//由构造函数可知这里的参数类型必须和泛型类型一致
ob1.print();
```
